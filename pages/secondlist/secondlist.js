// 连接云数据库
let db = wx.cloud.database()
// 检索数据表
let topicTable = db.collection("topic")


Page({

  /**
   * 页面的初始数据
   */
  data: {
      topicList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    let type = options.type;
    let level = options.level;
    if(type != undefined && level != undefined){
      topicTable.where({
        type,
        level
      })
      .get()
      .then(
        res => {
          // console.log(res);
          if(res.errMsg == "collection.get:ok"){
            console.log(res.data);
            that.setData({
               topicList: res.data
            })
          }
        }
      )
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})