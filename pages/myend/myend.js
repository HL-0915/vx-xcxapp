// 连接云数据库
let db = wx.cloud.database()
// 检索数据表
let topicTable = db.collection("topic")
Page({
  data: {
    v1: "",
    v2: "",
    level: "",
    type: "",
    num: 0,
    typeList: [
      {type:'html',text:"HTML"},
      {type:'css',text:"CSS"},
      {type:'js',text:"JavaScript"},
      {type:'pc',text:"计算机基础"},
      {type:'vue',text:"Vue"}
    ],
    isShow: false,
    message:"请选择"
  },
  addInput() {},
  addToggle(){
    this.setData({
      isShow:  !this.data.isShow
    })
  },
  addSelect(event){
    let _type = event.currentTarget.dataset.type;
    let _text = event.currentTarget.dataset.text;
    console.log(_type);
    this.setData({
      type: _type,
      message: "您选择的是"+_text,
      isShow: false
    })
  },
  addClick(event){
    let level = event.currentTarget.dataset.level;
    let num = event.currentTarget.dataset.num;
    // console.log(level,num);
    this.setData({
      level,
      num
    })
  },
  addPublish() {
    let question = this.data.v1; // 题目
    let answer = this.data.v2; // 答案
    let level = this.data.level; // 等级水平
    let type = this.data.type; // 类型 html css
    let hot = true;
    let like = 0;
    let time = new Date();
    let _id = Math.random().toString().slice(2) + "_" + new Date().getTime();
    let openid = wx.getStorageSync('_openid');
    if (question.length == 0 ||
      answer.length == 0 ||
      level.length == 0 ||
      type.length == 0) {
      wx.showToast({
        icon: "error",
        title: '输入框不能为空!'
      })
      return;
    }
    topicTable.add({
        data: {
          _id,
          question,
          answer,
          level,
          type,
          hot,
          like,
          time,
          openid
        }
      })
      .then(
        res => {
          console.log(res);
          wx.showModal({
            title: '发布成功,是否跳转?',
            success() {
              // 跳转页面
              wx.navigateTo({
                url: '/pages/topiclist/topiclist',
              })
            }
          })
        }
      )

      this.setData({v1:"",v2:""})
  }
})