// pages/search/search.js
let db = wx.cloud.database()
let subject = db.collection("topic")
Page({
  data: {
    inputV: '',
    resultList: [],
    backData: '',
    recWord: ["html", 'css', "js"],
    hisSearch: [],
    hasData: true,
  },
  confirm(e) {
    let v = e.detail.value;
    if (v == '') {
      wx.showToast({
        icon: 'error',
        title: '内容不能为空！',
      })
      return;
    }
    this.setLocal(v);
    this.getData(v);
    this.setData({
      inputV: ''
    })
  },
  getData(v) {
    wx.showLoading({
      title: '搜索中',
    })
    this.setData({
      hasData: false,
    })
    let typeList = [];
    let questionList = [];
    let answerList = [];
    let levelList = [];
    let resultData = [];
    let that = this;
    // 类型
    subject.where({
      type: db.RegExp({
        regexp: v,
        options: 'i',
      })
    }).get().then(
      res => {
        if (res.errMsg == "collection.get:ok") {
          typeList = res.data
          resultData = resultData.concat(typeList);
          // 问题
          subject.where({
            question: db.RegExp({
              regexp: v,
              options: 'i',
            })
          }).get().then(
            res => {
              if (res.errMsg == "collection.get:ok") {
                questionList = res.data
=                resultData = resultData.concat(questionList);
            
                subject.where({
                  answer: db.RegExp({
                    regexp: v,
                    options: 'i',
                  })
                }).get().then(
                  res => {
                    if (res.errMsg == "collection.get:ok") {
                      answerList = res.data
=                      resultData = resultData.concat(answerList);
                      // 等级
                      subject.where({
                        level: db.RegExp({
                          regexp: v,
                          options: 'i',
                        })
                      }).get().then(
                        res => {
                          if (res.errMsg == "collection.get:ok") {
                            levelList = res.data
                            // console.log(levelList);
                            resultData = resultData.concat(levelList);
                            // console.log(resultData);
                            resultData = that.removeSame(resultData);
                            that.setData({
                              resultList: resultData
                            })
                            if (resultData.length == 0) {
                              that.setData({
                                hasData: true
                              })
                            }
                            wx.hideLoading();
                          }
                        }
                      )
                    }
                  }
                )
              }
            }
          )
        }
      }
    )
    this.setData({
      backData: v,
    })
  },
  removeSame(arr) {
    let newarr = [];
    arr.forEach(element => {
      let isSame = true;
      for (let i = 0; i < newarr.length; i++) {
        if (element._id == newarr[i]._id) {
          isSame = false;
        }
      }
      if (isSame) {
        newarr.push(element);
      }
    });
    // console.log(newarr);
    return newarr;
  },
  clickRec(e) {
    // console.log(this.data.recWord[e.currentTarget.dataset.ind])
    let keyword = this.data.recWord[e.currentTarget.dataset.ind];
    this.setLocal(keyword)
    this.getData(keyword)
  },
  clickHis(e) {
    let keyword = this.data.hisSearch[e.currentTarget.dataset.ii];
    // console.log(keyword)
    this.setLocal(keyword)
    this.getData(keyword)
  },
  setLocal(v) {
    let arr = wx.getStorageSync('hisSear');
    // console.log(wx.getStorageSync('hisSear'))
    if (arr == '') {
      arr = [];
    }
    arr.unshift(v);
    arr = [... new Set(arr)];
    wx.setStorageSync('hisSear', arr);
    this.setData({
      hisSearch: arr
    });
  },
  delete() {
    wx.setStorageSync('hisSear', '')
    this.setData({
      hisSearch: []
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      hisSearch: wx.getStorageSync('hisSear')
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})