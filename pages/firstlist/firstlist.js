// 连接云数据库
let db = wx.cloud.database()
// 检索数据表
let topicTable = db.collection("topic")

Page({

  /**
   * 页面的初始数据
   */
  data: {
     topicArray: []
  },
  // 编写函数 用户获取不同分类的数据
  getTopicLevel(level){
      let  that = this;
      if(level){
          // 调用方法查询云数据库
          topicTable.where({
            level: level
          })
          .get()
          .then(
             res => {
              //  console.log("数据:",res);
              // 处理题目类型和题目数量
              if(res.errMsg == "collection.get:ok"){
                  // 接收数组
                  let arr = res.data;
                  // 空数组
                  let arr2=[];
                  // 定义空对象
                  let newObj = {};
                  for(let i = 0 ; i < arr.length ; i ++){
                      // 统计不同类型的题目出现的次数
                      if(newObj[arr[i].type] == undefined){
                        newObj[arr[i].type] = {}
                        newObj[arr[i].type].type = arr[i].type;
                        newObj[arr[i].type].level = arr[i].level;
                        newObj[arr[i].type].count = 1;
                      }else {
                        newObj[arr[i].type].count += 1;
                      }
                  }
                  // 循环newObj对象
                  // console.log(newObj);
                  for(let prop in newObj){
                    let obj = {}
                    if(prop.includes('html')){
                      obj.title = "HTML基础"
                    }
                    if(prop.includes('html1')){
                      obj.title = "HTML进阶"
                    }else 
                    if(/^css$/.test(prop)){
                      obj.title = "CSS基础"
                    }else 
                    if(prop.includes('css3')){
                      obj.title = "CSS3基础"
                    }else 
                    if(prop.includes('js') || prop.includes('javascript')){
                      obj.title = "javascript基础"
                    }else 
                    if(prop.includes('pc')){
                      obj.title = "计算机基础"
                    }
                    if(prop.includes('pc1')){
                      obj.title = "计算机中级"
                    }
                    if(prop.includes('pc2')){
                      obj.title = "计算机高级"
                    }
                    obj.count = newObj[prop].count;
                    obj.type = newObj[prop].type;
                    obj.level = newObj[prop].level;
                    arr2.push(obj);
                  }
                  // 打印数据
                  console.log(arr2);
                  that.setData({
                    topicArray: arr2 
                  })
              }
             }
          )
      }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      // 查询指定级别的数据
      this.getTopicLevel(options.level)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})