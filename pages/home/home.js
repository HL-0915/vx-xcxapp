// 连接云数据库
let db = wx.cloud.database()
// 检索需要操作数据表(集合)
let topicTable = db.collection("topic")

Page({
  data:{
    bannerList: [
      {
        src: "https://7368-shuatibao-0gkt03lc53d6d98a-1305236331.tcb.qcloud.la/banner4.png?sign=047bf09cc80ddccd841f60e19a4d028a&t=1641800222",
        text: "第一张",
        id: "xxx01"
      },
      {
        src: "https://7368-shuatibao-0gkt03lc53d6d98a-1305236331.tcb.qcloud.la/banner3.png?sign=a84d99f25a345dd06edb636960a3b481&t=1641800319",
        text: "第二张",
        id: "xxx02"
      },
      {
        src: "https://7368-shuatibao-0gkt03lc53d6d98a-1305236331.tcb.qcloud.la/banner2.png?sign=ad618b3b58570f622cec6c941c7fe998&t=1641800331",
        text: "第三张",
        id: "xxx03"
      },
      {
        src: "https://7368-shuatibao-0gkt03lc53d6d98a-1305236331.tcb.qcloud.la/banner1.png?sign=fb6a1145827d9cdd12a4f845975f944c&t=1641800339",
        text: "第四张",
        id: "xxx04"
      }
    ],
    navList: [],
    hotList:[]
  },
   // 编写方法 获取分类导航的
   getNavFunc(){
    // 记录当前函数作用域的this
    let that = this;
    // 调用云函数
    wx.cloud.callFunction({
      name: "getNav",// 云函数名称
      success: function(res){
          // 判断数据是否来自云端
          if(res.errMsg == "cloud.callFunction:ok") {
            // 赋值
            that.setData({
              navList: res.result
            })
            // 隐藏加载提示
            wx.hideLoading();
          }
      }
    })
 },
 // 编写方法 获取热门
 getHotFunc(){
  let that = this;
    // 记录当前函数作用域的this
    topicTable.where({hot:true})
    .get()
    .then(
      res => {
        console.log(res);
        //获取云数据库数据
        if(res.errMsg == "collection.get:ok"){
          let arr = res.data;
          console.log(arr)
          that.setData({
              hotList: arr 
          })
        }
      }
    )
 },
 // 初始化
 onLoad: function(){
  // 提示
  wx.showLoading({
    title: '正在加载...'
  })
  // 获取分类导航的数据
  this.getNavFunc();
  // 获取热门推荐的数据
  this.getHotFunc();
},

  

})