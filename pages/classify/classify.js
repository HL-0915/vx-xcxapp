// pages/classify/classify.js
let db = wx.cloud.database()
let subject = db.collection("topic")
Page({

    /**
     * 页面的初始数据
     */
    data: {
        ind : 0,
        allData : [],
        typeList : [],
        levelList : [],
    },
    click(e){
        let index = e.currentTarget.dataset.i
        this.setData({
            ind : index
        })
        this.getLevelList(this.data.typeList[index])
    },
    getAlldata(){
      this.setData({
        allData : []
      })
      let that = this;
      wx.showLoading({
        title: '加载中',
      })
        subject.count()
      .then(
        async res => {
          let total = res.total; 
          let len = Math.ceil(total / 20); 
          for (let i = 0; i < len; i++) { 
            await subject.skip(i * 20).limit(20)
              .get()
              .then(
                async res => {
                  let new_data = res.data;
                  let old_data = that.data.allData;
                  that.setData({
                    allData: old_data.concat(new_data)
                  })
                  if(that.data.allData.length == total){
                    that.getTypeList(that.data.allData);
                    that.getLevelList(that.data.typeList[0])
                    wx.hideLoading()
                  }
                }
              )
          }
        }
      )
    },
    getTypeList(arr){
      let typeArr = []
      arr.forEach((element,index) => {
        let isSame = false;
        let sameIndex = 0;
        for(let i = 0;i<typeArr.length;i++){
            if(element.type == typeArr[i][0].type){
              isSame = true;
              sameIndex = i;
            }
        }
        if(isSame){
          typeArr[sameIndex].push(element);
        }else{
          let newarr = [];
          newarr.push(element);
          typeArr.push(newarr);
        }
      });
      this.setData({
        typeList : typeArr
      })
    },
    getLevelList(arr){
      let levelArr = []
      arr.forEach((element,index) => {
        let isSame = false;
        let sameIndex = 0;
        for(let i = 0;i<levelArr.length;i++){
            if(element.level == levelArr[i][0].level){
              isSame = true;
              sameIndex = i;
            }
        }
        if(isSame){
          levelArr[sameIndex].push(element);
        }else{
          let newarr = [];
          newarr.push(element);
          levelArr.push(newarr);
        }
      });
      this.setData({
        levelList : levelArr
      })
      // console.log(this.data.levelList)
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
      this.getAlldata();
      this.setData({
        ind : 0
      })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})