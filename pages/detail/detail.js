// 连接云数据库
let db = wx.cloud.database()
// 检索需要操作数据表(集合)
let topicTable = db.collection("topic")

// pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
      question: "正在加载...",
      answer: "正在加载..."
  },
  // 编写方法 读取指定id的数据
  getTopicData(id){
    if(id){
      let that = this;
      topicTable.where({_id: id})
      .get()
      .then(
        res => {
          // console.log('2.0',res);
          if(res.errMsg == "collection.get:ok"){
            // 取值
            let question = res.data[0].question;
            let answer = res.data[0].answer;
            // 存值
            that.setData({
              question: question,
              answer: answer
            })
          }
        }
      )
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      console.log('1.0---',options);
      // 记录首页传递过来的参数
      let id = options.id;
      // 查询云数据库的数据
      this.getTopicData(id);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})