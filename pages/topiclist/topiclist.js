// 连接云数据库
let db = wx.cloud.database()
// 检索数据表
let topicTable = db.collection("topic")

Page({
  data: {
    topicArray: []
  },

  // 管理题目
  // 获取所有题目数据
  getTopicData(){
    topicTable.get()
    .then(
      res => {
        // console.log(res);
        if(res.errMsg == "collection.get:ok"){
          this.setData({
              topicArray: res.data
          })
        }
      }
    )
  },
  
  // 删除指定的题目
  removeTopic(event) {
    let that = this;
    // 获取自定义属性
    let id = event.currentTarget.dataset.id;
    let _openid = wx.getStorageSync('_openid');
    console.log(id);
    console.log(_openid);
    topicTable.where({
      _id: id,
      _openid: _openid
    }).remove().then(
      res => {
        console.log(111);
        console.log(res);
        if (res.errMsg == "collection.remove:ok") {
          wx.showToast({
            title: '删除成功',
          })
          this.getTopicData();
        }
      }
    )
  },
  onLoad: function () {
    this.getTopicData()
  }
})