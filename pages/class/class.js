// pages/mine/mine.js
let db = wx.cloud.database()
let subject = db.collection("subject")
Page({

    /**
     * 页面的初始数据
     */
    data: {
    userInfo: {},
    hasUserInfo: false,
    canIUseGetUserProfile: false,
    isShow: true,
    avatarUrl: "",
    nickName : '',
    },
    setUserInfo(option){
        // 调用用户信息的云函数
        wx.cloud.callFunction({
         name:'getUser',
         data: option,
         success(res){
           console.log(res);
         }
       })
     },
     getUserProfile(e) {
        // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
        // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
        wx.getUserProfile({
          desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
          success: (res) => {
            // console.log(res.userInfo.nickName);
            let nickName = res.userInfo.nickName;
            let avatarUrl = res.userInfo.avatarUrl;
            wx.setStorageSync('avatarUrl', avatarUrl)
            wx.setStorageSync('nickName', nickName)
            wx.setStorageSync('isShow', true)
            this.setUserInfo({nickName})
            this.setData({
              userInfo: res.userInfo,
              hasUserInfo: false,
              avatarUrl: avatarUrl,
              isShow: false,
              nickName : nickName
            })
          }
        })
      },
      getUserInfo(e) {

        this.setData({
          userInfo: e.detail.userInfo,
          hasUserInfo: true
        })
      },
      exit(){
        wx.setStorageSync('isShow', false);
        this.setData({
          isShow: true,
        })
        // this.onShow();
      },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        if (wx.getUserProfile) {
            this.setData({
              canIUseGetUserProfile: true
            })
          }
          let isShow = wx.getStorageSync('isShow')
          let avatarUrl = wx.getStorageSync('avatarUrl')
          let nickName = wx.getStorageSync('nickName')
          if(isShow){
            this.setData({
              isShow: false,
              avatarUrl: avatarUrl,
              nickName: nickName
            })
          }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})