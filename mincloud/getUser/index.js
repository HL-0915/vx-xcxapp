// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: "shuatibao-0gkt03lc53d6d98a"
})

let db = cloud.database({
  env: "shuatibao-0gkt03lc53d6d98a"
});

let usersTable = db.collection("users")
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const nickName = event.nickName || "admin2022"
  let id = wxContext.OPENID;
  // md5(id)
  // 查询数据
  
  // ...  
  // 添加用户
  return await usersTable.add({
    data: {
      _id: id,
      name: nickName
    }
  })
}