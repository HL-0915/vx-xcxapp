// 云函数入口文件
const cloud = require('wx-server-sdk')

// cloud.init({env:"shuatibao-0gkt03lc53d6d98a"})
cloud.init()


// 云函数入口函数
// event    用于接收小程序传递的参数  例如： let a = event.a ;
// context  关于微信账号信息的对象  例如，appid  openid  
exports.main = async (event, context) => {
  // 每次涉及云函数代码的修改
  // 必须上传云函数（可以不传node_modules）
  // 导航数据
  let navArray = [
      {
        id: "21221",
        text: "初级前端",
        src:"https://636c-cloud1-5g9pip1med783b8d-1309100387.tcb.qcloud.la/chu2.png?sign=61beca0575ae5c145ad149934efd53af&t=1642531372"
      },
      {
        id: "21222",
        text: "中级前端",
        src: "https://636c-cloud1-5g9pip1med783b8d-1309100387.tcb.qcloud.la/zhong2.png?sign=82b112ffe4b9ee69fd959ebcdbde67a8&t=1642531407"
      },
      {
        id: "21223",
        text: "高级前端",
        src: "https://636c-cloud1-5g9pip1med783b8d-1309100387.tcb.qcloud.la/gao2.png?sign=8235251f17ca560760e41d15d5af8a5f&t=1642531428"
      },
      {
        id: "21224",
        text: "计算机基础",
        src: "https://636c-cloud1-5g9pip1med783b8d-1309100387.tcb.qcloud.la/pc2.png?sign=817d4f79326ec80a27a0f92a8a25ddee&t=1642531442"
      }
  ]
  console.log(navArray);
  // 可以返回数据
  // 也可以promise实例
  return navArray;
}
