// components/banner/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        bannerArray: {
            type: Array
          }

    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        changeIndex(e){
            let current = e.detail.current;
            this.setData({
              num: current
            })
         }
    }
})
