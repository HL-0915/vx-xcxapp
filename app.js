// app.js
App({
  onLaunch() {
    //初始化云开发环境
    wx.cloud.init({
      env:"cloud1-5g9pip1med783b8d"
    })
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)


  // 设置openid
  wx.cloud.callFunction({
    name:'getOpenid',
    success:function(res){
      console.log(res);
      wx.setStorageSync('_openid', res.result.openid)
    }
  })

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
  },
  globalData: {
    userInfo: null
  },
  // //下拉刷新
  //   onPullDownRefresh:function()
  //   {
  //     wx.showNavigationBarLoading() //在标题栏中显示加载
      
  //     //模拟加载
  //     setTimeout(function()
  //     {
  //       // complete
  //       wx.hideNavigationBarLoading() //完成停止加载
  //       wx.stopPullDownRefresh() //停止下拉刷新
  //     },1500);
  //   }
})
